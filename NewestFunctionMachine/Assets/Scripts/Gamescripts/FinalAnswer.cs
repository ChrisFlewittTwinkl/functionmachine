﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalAnswer : MonoBehaviour
{
    public NumberChanger firstNum, secondNum;

    private float firstNumValue, secondNumValue;

    public OperatorChange opChan;

    public float answerNum;
    public Text answerText;

    public FinalAnswer previousAnswer,nextAnswer;
    // Start is called before the first frame update
    private void Start()
    {
        QuestionMark();
    }

    public void ShowAnswer()
    {
        answerText.text = answerNum.ToString();
    }

    public void QuestionMark()
    {
        answerText.text = "?";
    }
    
    
}
