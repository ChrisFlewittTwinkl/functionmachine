﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SumManager : MonoBehaviour
{
    public NumberChanger[] numbers;
    public OperatorChange[] operators;
    public FinalAnswer[] finalAnswers;

    public float returnValue;


    public void UpdateMaths()
    {
        
        finalAnswers[0].answerNum = WorkingOut(numbers[0].number, numbers[1].number, operators[0].operatorNum);
        finalAnswers[0].QuestionMark();
        if (finalAnswers.Length > 1)
        {
            finalAnswers[1].answerNum = WorkingOut(finalAnswers[0].answerNum, numbers[2].number, operators[1].operatorNum);
            finalAnswers[1].QuestionMark();
            if (finalAnswers.Length > 2)
            {
                finalAnswers[2].answerNum = WorkingOut(finalAnswers[1].answerNum, numbers[3].number, operators[2].operatorNum);
                finalAnswers[2].QuestionMark();
            }
        }
    }

    public float WorkingOut(float first, float second, int operation)
    {
        
        switch (operation)
        {
            case 0:
                returnValue = (first + second);
                break;
            case 1:
                returnValue = (first - second);
                break;
            case 2:
                returnValue = (first * second);
                break;
            case 3:
                returnValue = (Mathf.Round(((float) first / (float) second) * 100.0f) / 100.0f);
                break;
            default:
                break;
        }

        return returnValue;

    }

    public void RandomStuff()
    {
        foreach (var num in numbers)
        {
            num.number = Random.Range(0, 9);
            num.UpdateText();
        }

        foreach (var op in operators)
        {
            op.operatorNum = Random.Range(0, 3);
            op.WorkItOut(op.operatorNum);
        }
        
        UpdateMaths();
    }

    public void ResetButton ()
    {
        foreach (var num in numbers)
        {
            num.number = 0;
            num.UpdateText();
        }

        foreach (var op in operators)
        {
            op.operatorNum = 0;
            op.WorkItOut(op.operatorNum);
        }
        
        UpdateMaths();


    }
    
}
