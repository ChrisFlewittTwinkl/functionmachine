﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSelector : MonoBehaviour
{
    public GameObject[] modes;

    public SumManager[] sumMan;

    public Animator[] modeAnims;

    private int sumManId;
    // Start is called before the first frame update
    void Start()
    {
        DisAbleModes();
        modeAnims[0].SetBool("In",true);

    }

    public void EnableMode(int chosenMode)
    {
        DisAbleModes();
        modes[chosenMode].SetActive(true);
        modeAnims[chosenMode].SetBool("In",true);
        
        sumManId = chosenMode;
    }

    private void DisAbleModes()
    {
        foreach (var obj in modeAnims)
        {
            obj.SetBool("In",false);
        }
    }

    public void Randomise()
    {
        sumMan[sumManId].RandomStuff();
    }
}
