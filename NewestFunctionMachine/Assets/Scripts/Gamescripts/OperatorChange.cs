﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class OperatorChange : MonoBehaviour
{
    // Start is called before the first frame update



    public int operatorNum;
    public Text operatorText;
    public NumberChanger numchange;
    public SumManager sumMan;
    
    
    void Start()
    {
        operatorNum = 0;
        operatorText.text = "+";
    }

    // Update is called once per frame
    public void ChangeOperator(bool upOperator)
    {
        
        
        if (upOperator)
        {
            operatorNum++;
            if (operatorNum > 3)
            {
                operatorNum = 0;
            }
        }
        else
        {
            operatorNum--;
            if (operatorNum < 0)
            {
                operatorNum = 3;
            }
        }
        WorkItOut(operatorNum);
        sumMan.UpdateMaths();
    }

    public void WorkItOut(int op)
    {
        switch(op) {
            case 0:
                operatorText.text = "+";
                break;
            case 1:
                operatorText.text = "-";
                break;
            case 2:
                operatorText.text = "×";
                break;
            case 3:
                operatorText.text = "÷";
                break;
            default:
                break;
        }
    }
    
    
    
    
    
    
}
