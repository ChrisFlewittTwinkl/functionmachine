﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberChanger : MonoBehaviour
{
    public int number;

    public Text numberText;

    public SumManager sumMan;
    
    // Start is called before the first frame update
    void Start()
    {
        numberText.text = number.ToString();
    }

    public void ChangeNumber(bool add)
    {

        if (add)
        {
            number++;
        }
        else
        {
            number--;
        }
        UpdateText();
        sumMan.UpdateMaths();
    }

    public void UpdateText()
    {
        numberText.text = number.ToString();
    }
    
}
