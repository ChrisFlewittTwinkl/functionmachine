﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public GameObject pauseButton;
    public GameObject PausePanel;
    public GameObject instructPanel;
    public GameObject EndPanel;

    public bool gameScreen;

    public bool doYouWantToDisableTransPanel;
    

    // Start is called before the first frame update
    void Start()
    {

       if(gameScreen){
           pauseButton.SetActive(false);
           InstructionEnable(false);
           EndPanel.SetActive(false);

       }
       
    }

    public void Pause (){
        pauseButton.SetActive(false);
        PausePanel.SetActive(true);
        
    }

    public void UnPause (){
        pauseButton.SetActive(true);
        PausePanel.SetActive(false);
        
    }

    public void InstructionEnable(bool fromPause){
        
        instructPanel.SetActive(true);
        
        if(fromPause == true){
        PausePanel.SetActive(false);
        }
    }

    public void CloseInstructions(){
        instructPanel.SetActive(false);
        
        pauseButton.SetActive(true);
    }

    public void EndScreen (){
        EndPanel.SetActive(true);
        pauseButton.SetActive(false);
        
    }

}
